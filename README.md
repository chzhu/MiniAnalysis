# MiniAnalysis
A fairly simple and mini, json config based Analysis for lopping trees. It provide many flexible tools to help to do such as cutflow, slimming trees and make histograms.
It is also designed to be easier to support different kinds of analysis trees. Currently the [TauCP](https://gitlab.cern.ch/ATauLeptonAnalysiS/xTauFramework/tree/r21_v10) trees
and [XAMPPstau](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau) trees are supported. It's also easy for user to create his own tree support by inheriting the `class AnaTree`.

## To compile
Currently the code could be compiled with the SLC7 cvmfs enironment. If you want to build without this enironment, you should be sure to use gcc version > 8 to use the c++17 feature. 
Besides, you need to install this two dependences: `Root` and `Boost`.
Then modify the code about finding `Root` and `Boost` in the file `CMakeLists.txt` at line from `9-15`

It's quite simple to compile with SLC7 cvmfs enironment:
```bash
setupATLAS									#setupATLAS if not yet
# Code build passed under SLC7 Root 6.22.00. In general it will not fail on any root version using gcc8. 
# But use root version as higher as possiable to avoid potential multi-thread issue from ROOT side
lsetup "root 6.22.00-x86_64-centos7-gcc8-opt" 
lsetup cmake								#Required cmake with version > 2.8. Don't need to do this if yo already have
mkdir build
cd build
cmake ../ && make -j$(echo $(nproc)-1 | bc)
```
Then the program will be built named `mini_analysis`.

## To run

To run, you can get the help of the code by just typing `./mini_analysis -h`
```text
[username@server build]$./mini_analysis -h
[INFO | 22:55:02 | main.cxx:45] ************************** Analysis Start **************************
A fairly simple and mini Analysis:
  -h [ --help ]            print this message and exit
  -l [ --listAnalysises ]  list all of the supported analysises and exit
  -v [ --verbose ]         show debug and verbose log messages
  -a [ --analysis ] arg    choose the analysis you want to use
  -c [ --config-file ] arg The config file you will use. Note the program 
                           option will override the setting in the config file
  -o [ --output ] arg      Output name - if not supplied, will use the name of 
                           the first inputFile with removing dir path
  -i [ --input-files ] arg Comma-separated list of input files
  -w [ --weight ] arg      Optional: Don't use default sample weight and define
                           your own fill weight
  -t [ --thread ] arg      Optional: When running multiple trees. How many 
                           thread you want to use to speed up. Default: 1
  -n [ --MakeNtuple ]      Fill ntuple

[INFO | 22:55:02 | main.cxx:52] ********** Analysis End. Total run time is: 0ms ***********
```
You can get some hint from the help. To get the supported analysis, you could use `./mini_analysis -l`.

For a normal run, usually these options are used
```text
[username@server build]$./mini_analysis -a [a_supported_analysis] -c [a_json_config_file] -i inputFile1,inputFile2...
```
You could also use `-o $output` to define the output file name. If you want to enable the making tree ntuple, you could add `-n` option (This is currently buggy and will always write ntuple!).
When you are processing files with multiple trees, you may would like to use `-t $threadNum` to use multiple threads to run these trees in parallel.
Sometimes we want to see the raw events after some selection, for this case you could use `-w $hardcodeWeight` to define a hardcode weight to override the deafult filling weight.
If you face some bugs in the code, you could use `-v` to show the debug message for more details.

The config file is using a fairly simple json file. You could get some samples at `config` folder.
```json
{
	"MakeNtuple": true,
	"Trees": ["NOMINAL"],
	"tauID": "medium",
	"MetaData": ["./Metadata.json","/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt"]
}
```
In a config, you must provide a object named "Trees" which is the one you want to loop. It's a array object and it support the [regex](https://en.wikipedia.org/wiki/Regular_expression). 
Such as if you write `"Trees": [".*Staus.*","MetadataTree"],`, it will loop all of the trees contains "Staus" in its name and another tree named "MetadataTree".

Another object must be provided is "MetaData". It supported json format Metadata (A example is shown at config/MetadataExample.json) and PMG official metadata. If there are overlap metadata in those files, it will take the later json file value, then front json file value. 
The PMG official metadata file is always taken into count as the last order due to it's too large and it cost huge time to loop the file.

## To write your own analyses

It's suggested to put your own code to the userInclude folder and userSrc and name your cpp source file name same as your analysis name for better code structure.

### Write your own tree

One could create his own tree by using Root official code `tree->MakeClass("treeName")`. Once the header file and source file created, you could just delete the 
source file because we don't need it. Then you could adjust your header file by adding the inherition of the class AnaTree. 
Usually you just need the branch declearation and Init function and could safely delete other functions. 
You could also refering the structure of the existing tree for more hints: `SubTauTree.h` and `XamppTree.h`

### Write your own analysis code

Two header files are essentially need for your cpp source file: `AnaRunner.h` and the tree header file you want to use. Youd could also include other header files for
more tools. Then you could define your analysis by using just one line `DefineLooper($YourAnalysisName, $AnalysisTreeClassName);`. Then you could perform your analyssis by writing function `void $YourAnalysisName::loop(){}`

Currently the analysis provide many tools for slimming, histogram producing, cutflow and event yield production. The CutCount is still uncer Dev and you could not use it now.

### Automatically histogram/tree fill by using powerful Cutflow and Region tool
Usually in the many analysises we want to do is just the tree slimming and histogram filling. In traditional way to do this we must first declear the tree and histogram
before the event loop, then make the filling by calling the function like `tree->Fill()` and `hist->Fill(var, weight)`. But it's kind of waste to use two lines for sepecific. 
Besides, while doing the cutlow or even N-1 plot, we will face the problem that we must create many if statement for each kinds of histogram filling. Though it's easy 
to design a cutflow class, it's not a easy to add N-1 plot and any kinds of the histograms for a cut step. The core problem is the histogram filling need extra informations
from var and weight which make the process calling could not be done in a loop.

Luckily, in C++11, the std::function is introduced, which let the histogram auto fill possiable. (The variable address could also perform similiar thing but much more dangerous.)
We could first declear a variable holder, then assign each histogram this holder with a lambda function `[&]{return func(holder);};` Usually we just need the var so the
lambad function is just `[&]{return holder;};`, but in some case you could really perform some function for this holder for some complicate usage. (And this is another
adventage of using lambda function compared with using variable address). Thus we could do something like `Hist myhist(name, nBin, binStart, binEnd, [&]{return fillVar;},[&]{return weight;})` 
and let the histogram filling become something like `void Hist::fill(){h->Fill(fillVar(),wei())}`. Then we could fill all of the histograms in one loop by just calling

```c++
for(auto hist : hists){
	hist->fill();
}
```

This idea is implemented by most of the tools in this analysis. When register cutflow and region, the cut for the cutflow and region is also a holder. Like
```c++
mCutflow->registerCut("1Lepton",[&] {return Var["nLep"] == 1;});
Region* regionSR_Data = addRegion([&,i]{return (Var["isData"] && Var["SR"] && Var["tauID"] && Var["RecoMode"] == i);});
```
So when doing the analysis, what we do before the loop is to declear the variable holder and declear the cutflow/region/histograms with the holder.
Then in the loop we just update the values of the holder, then make the all filling by calling just one or two functions.

The details of the idea is implemented in the `Hist.h`, `Cutflow.h` and `Region.h`. The only difference between the idea and the implement is, in the implement the weight
is hold by the Region or Cutflow object not the Hist object because the Cutflow and Region still need this info to do the yields calculation. 
When doing the analysis, first you should declear the Variable holder, then you could set the global weight by using
```c++
setWeight(std::function<double()> weight);
```

You could add the cutflow and region by using these functions:
```c++
//For sepecific usage, willFillHist could also choose Cutflow::NO_HIST, histDirName is the Dir you want to store in the root file, default means the root folder
auto mCutflow = addCutflow(); //Function is Cutflow* addCutflow(bool willFillHist = Cutflow::SHOW_HIST, std::string histDirName = ""); 
auto region = addRegion(std::function<bool()> cut, "folder1");	//Function is Region* addRegion(std::function<bool()> cut, std::string histDirName = "");

//If needed you could also set sepecific weight for a cutflow/region. Usually this is for syst tree/histogram
mCutflow->setWeight(anotherWeightFunc);
region->setWeight(anotherWeightFunc);
```

Then you could register your cut, histograms to the cutflow and regions
```c++
auto firstcut = mCutflow->registerCut("1Lepton",[&] {return Var["nLepton"] == 1;});
firstcut->addHist("tau1Pt", 20, 0, 400, [&] {return Var["tau1Pt"]; }, Hist::USE_OVERFLOW); // 'Hist::USE_OVERFLOW' is optional, if add this, will add the overfolw to the last bin
//You could also add a cutflow with N-1 distribution added for this cut by adding needed histogram infos
mCutflow->registerCut("bVeto",[&] {return Var["bVeto"];},"bTag",2,0,2,[&]{return !Var["bVeto"];});

region->addHist("var",10,0,200,[&]{return Var1;});
```

In the loop, what you need is just update the values of each variable, then call function `fillRegions();` and `fillCutflows();`

### Print Out cutflow and region result

After the whole loop, you could print the whole cutflow and the regions by using lines like:

```c++
// for each cutflow/region, you must sepecific a output file name for it. Now simply use cutflow"$i" to seperate. 
// For each sample, will use the output file name by default. You could also use mCutflow->setName(name) to reanme the sample name
int i = 0;
for(auto c : getCutflows()){
	c->PrintOut("cutflow"+std::to_string(i)); 
	i++;
}
// Print out regions is quite simple, you could directly sepecific the output file name and sample name
int j = 0;
for(auto region : getRegions()){
    region->printOut("yield"+std::to_string(j),sampleName);
    j++;
} 
```

### Do the tree slimming using Cutflow tool

Due to the tree slimming is actually use a set of cut to do the slimming. So we use the cutflow to do the tree slimming. You just need to create a tree using function
`newTree(std::string treeName)` and use function `setFillTree(TTree* tree)` let the cutflow know it should fill this tree after a passing set of cut.

```c++
auto outTree = newTree(treeName);
outTree->Branch("branch1",&var1);
outTree->Branch("branch2",&var2);
outTree->Branch("branch3",&var3);
mCutflow->setFillTree(outTree);
```

Sometimes what we want to do is fill a tree with the same structure as the original tree. For this case you could use `cloneCurrentAnaTree(std::string newname = "", Long64_t nentries = 0, Option_t *option = "")`
to do the tree structure copying. As you see in the function structure, you could rename the tree a name if you want(If you don't provide it it will use the old tree name).
The other two arguement is the advanced arguement which may passed to the `tree->CloneTree()` when doing the tree copying.

```c++
auto truth1Tree = cloneCurrentAnaTree("Truth1_"+currentTreeName());
truth1Tree->Branch("newbranch1",&var1);
truth1Tree->Branch("newbranch2",&var2);
truth1CutSet->setFillTree(truth1Tree);
```

### Examples

There are some examples for you to get some hint for writing your own analysis codes. They are currently at `userSrc/`. They are actually all some analysises code
now are under using. 

The `userSrc/SubTau_PlotPreBDT.cxx` would be a good example for region using and region histogram plotting. 

The `userSrc/Wh_Slimmer.cxx` and `userSrc/SubTau_HFTree_Sig` would be a good example for tree slimming and cutflow histogram plotting.

## Thanks

[SimpleAnalysis](https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis) for many great ideas about object definitions, many many useful functions, 
arguement parsering and multiple analysises running.

[JSON for Modern C++](https://github.com/nlohmann/json) for a great c++ json parser with really simple interface and good speed performance which is used in many
situations of the code.

[pugixml](https://github.com/zeux/pugixml) for a simple and fast XML parser which is used to parser the GRL.

[easyloggingpp](https://github.com/amrayn/easyloggingpp) for a light-weight and fast performing logging framework which is used for logging.
