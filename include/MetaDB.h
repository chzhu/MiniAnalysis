#pragma once
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "easylogging++.h"
#include "json.hpp"

class MetaDB {
    using json = nlohmann::json;

public:
    // Usage: MetaDB::Instance().dosomething();
    static MetaDB& Instance() {
        static MetaDB mMetaDB;
        return mMetaDB;
    }
    void loadMetaDBFile(std::filesystem::path dbFile);
    void resetMetaDBFile(std::filesystem::path dbFile);
    double getLumi(int MCcompaign);
    double getXsection(int mcChannel, int SUSYFinalState);
    double getNnorm(int MCcompaign, int mcChannel, int SUSYFinalState);
    double getkFactor(int mcChannel);
    double getFilterEff(int mcChannel);
    /*Get a certen weight for sepecific mcChannel and MCcompaign. The default SUSY final state is set to 0(no SUSY final
     * state process). If the mcChannel is 0, it will be treated as data and will definitely return 1. If needed, one
     * could summrize more sepecific weights like genWeights, EleWeights and etc to an extraWeight and put it into the
     * arguement.
     * */
    double getWeight(int MCcompaign, int mcChannel, int SUSYFinalState = 0, double extraWeight = 1);

private:
    MetaDB() {
        metadata = {};
        PMGmetadata = {};
        PMGDBfile = "";
        jsonUpdated = false;
        updatejsonStamp = "";
    };
    MetaDB(const MetaDB&);            // forbiden in singleton
    MetaDB& operator=(const MetaDB&); // forbiden in singleton
    ~MetaDB() {
        if (jsonUpdated == true) {
            std::string outJsonName = "Meta_update_" + updatejsonStamp + ".json";
            std::ofstream o(outJsonName);
            o << std::setw(4) << metadata << std::endl;
        }
    }
    void constructPMGMeta();

    json metadata;
    bool jsonUpdated;
    std::filesystem::path PMGDBfile;
    json PMGmetadata;
    std::string updatejsonStamp;
};
