#pragma once

// The interface class for the threadPool runnable. There should not be any non-virtual funcs in this class to avoid issues from multiple inheritance
class Runnable {
public:
    virtual void run() = 0;
    virtual ~Runnable() = default;
};