#include "MetaDB.h"

void MetaDB::loadMetaDBFile(std::filesystem::path dbFile) {
    CHECK(exists(dbFile) && is_regular_file(dbFile)) << "MetaDB File " << dbFile << " not Found! Loading terminated";
    std::ifstream cFile(dbFile);
    if (json::accept(cFile)) {
        cFile.clear();
        cFile.seekg(0, std::ios_base::beg);
        if (metadata.empty()) {
            cFile >> metadata;
        } else {
            json tmp;
            cFile >> tmp;
            metadata.merge_patch(tmp);
        }
    } else {
        std::string tmp;
        std::getline(cFile, tmp);
        // check if if the first line match the PMG data file
        if (tmp.find("genFiltEff") != std::string::npos && tmp.find("crossSection") != std::string::npos) {
            PMGDBfile = dbFile;
        } else {
            LOG(ERROR) << "File " << dbFile << " is not a valid json file or PMG Metadata DB file! Skip it.";
        }
    }
}
void MetaDB::resetMetaDBFile(std::filesystem::path dbFile) {
    CHECK(exists(dbFile) && is_regular_file(dbFile)) << "MetaDB File " << dbFile << " not Found! Loading terminated";
    std::ifstream cFile(dbFile);
    if (json::accept(cFile)) {
        cFile.clear();
        cFile.seekg(0, std::ios_base::beg);
        cFile >> metadata;
    } else {
        std::string tmp;
        std::getline(cFile, tmp);
        // check if if the first line match the PMG data file
        if (tmp.find("genFiltEff") != std::string::npos && tmp.find("crossSection") != std::string::npos) {
            PMGDBfile = dbFile;
            constructPMGMeta();
            this->metadata = this->PMGmetadata;
        } else {
            LOG(ERROR) << "File " << dbFile << " is not a valid json file or PMG Metadata DB file! The Metadata DB reset failed.";
        }
    }
}

double MetaDB::getWeight(int MCcompaign, int mcChannel, int SUSYFinalState, double extraWeight) {
    // return 1 if it's data (mcChannel == 0)
    if (mcChannel == 0) return 1;
    return getLumi(MCcompaign) * getXsection(mcChannel, SUSYFinalState) * getFilterEff(mcChannel) * getkFactor(mcChannel) * extraWeight /
           getNnorm(MCcompaign, mcChannel, SUSYFinalState);
}

double MetaDB::getLumi(int MCcompaign) {
    double lumi(1);
    std::string compaign = std::to_string(MCcompaign);
    if (metadata["lumi"][compaign].empty()) {
        LOG(WARNING) << "Warnning! No lumi is set! Set to 1.";
    } else {
        lumi = metadata["lumi"][compaign];
    }
    return lumi;
}
double MetaDB::getXsection(int mcChannel, int SUSYFinalState) {
    std::string channel = std::to_string(mcChannel);
    std::string finalState = std::to_string(SUSYFinalState);
    double xSec(0);
    if (metadata["xSection"][finalState][channel].empty()) {
        // If SUSYFinalState is 0, check the PMGDBfile for more info
        if (SUSYFinalState == 0 && PMGDBfile != "") {
            LOG(INFO) << "No xSection found in json DB file, searching at " << PMGDBfile;
            // Construct PMGmetadata if we haven't do it
            if (PMGmetadata.empty()) {
                constructPMGMeta();
            }
            // Return metadata and update the initial metadata if it is found
            if (!PMGmetadata["xSection"]["0"][channel].empty()) {
                xSec = PMGmetadata["xSection"]["0"][channel];
                metadata["xSection"]["0"][channel] = xSec;
                jsonUpdated = true;
                updatejsonStamp = channel;
            } else {
                LOG(ERROR) << "Warnning! No xSection is found for mcChannel " << mcChannel << " and SUSYFinalState " << SUSYFinalState
                           << "! Set to 0.";
            }
        } else {
            LOG(ERROR) << "Warnning! No xSection is found for mcChannel " << mcChannel << " and SUSYFinalState " << SUSYFinalState << "! Set to 0.";
        }
    } else {
        xSec = metadata["xSection"][finalState][channel];
    }
    return xSec;
}
double MetaDB::getNnorm(int MCcompaign, int mcChannel, int SUSYFinalState) {
    std::string compaign = std::to_string(MCcompaign);
    std::string channel = std::to_string(mcChannel);
    std::string finalState = std::to_string(SUSYFinalState);
    float nNorm(0);
    if (metadata["Nnorm"][finalState][channel][compaign].empty()) {
        LOG(ERROR) << "Warnning! No nNorm is found for mcChannel " << mcChannel << " and SUSYFinalState " << SUSYFinalState << "! Set to 0.";
    } else {
        nNorm = metadata["Nnorm"][finalState][channel][compaign];
    }
    return nNorm;
}
double MetaDB::getkFactor(int mcChannel) {
    std::string channel = std::to_string(mcChannel);
    float kFactor(1);
    if (!metadata["kFactor"][channel].empty()) {
        kFactor = metadata["kFactor"][channel];
    } else {
        if (PMGDBfile != "") {
            LOG(INFO) << "No kFactor found in json DB file, searching at " << PMGDBfile;
            // Construct PMGmetadata if we haven't do it
            if (PMGmetadata.empty()) {
                constructPMGMeta();
            }
            // Return metadata and update the initial metadata if it is found
            if (!PMGmetadata["kFactor"][channel].empty()) {
                kFactor = PMGmetadata["kFactor"][channel];
                metadata["kFactor"][channel] = kFactor;
                jsonUpdated = true;
                updatejsonStamp = channel;
            }
        }
    }
    return kFactor;
}
double MetaDB::getFilterEff(int mcChannel) {
    std::string channel = std::to_string(mcChannel);
    float filterEff(1);
    if (!metadata["filterEff"][channel.c_str()].empty()) {
        filterEff = metadata["filterEff"][channel.c_str()];
    } else {
        if (PMGDBfile != "") {
            LOG(INFO) << "No filterEff found in json DB file, searching at " << PMGDBfile;
            // Construct PMGmetadata if we haven't do it
            if (PMGmetadata.empty()) {
                constructPMGMeta();
            }
            // Return metadata and update the initial metadata if it is found
            if (!PMGmetadata["filterEff"][channel].empty()) {
                filterEff = PMGmetadata["filterEff"][channel];
                metadata["filterEff"][channel] = filterEff;
                jsonUpdated = true;
                updatejsonStamp = channel;
            }
        }
    }
    return filterEff;
}

void MetaDB::constructPMGMeta() {
    std::ifstream cFile(this->PMGDBfile);
    CHECK(cFile) << "PMG DB File " << this->PMGDBfile << " not Found! Loading terminated";
    std::string temp;
    std::getline(cFile, temp); // Skip first line
    json jTemp;
    while (std::getline(cFile, temp)) {
        std::istringstream Line(temp);
        /*Line format:
        dataset_number physics_short crossSection genFiltEff kFactor relUncertUP relUncertDOWN generator_name etag
        450764 Py8EG_A14NNPDF23LO_XHS_X3000_S2500_bbWW_1lep 4.9549e-18 4.3860E-01 1.0 0.0 0.0 Pythia8.235+EvtGen.1.6.0
        e7586
        */
        double xSec, filterEff, kFactor;
        std::string MCName;
        int MCID;
        Line >> MCID >> MCName >> xSec >> filterEff >> kFactor;
        std::string s_MCID = std::to_string(MCID);
        jTemp["xSection"]["0"][s_MCID] = xSec;
        jTemp["filterEff"][s_MCID] = filterEff;
        jTemp["kFactor"][s_MCID] = kFactor;
    }
    PMGmetadata = jTemp;
}
